# !env/bin/python3
# coding: utf-8
# @author:blghn - fnym

from tkinter import *
from tkinter import ttk
from urllib.request import urlopen
from functools import partial
from tkinter import messagebox
from threading import Thread
import urllib as u
from queue import Queue
import json
import socket


class Window1:
    def __init__(self, master, text, pText, pTitle, pBilgi, fonksiyon=None, no=0, command=None, ):
        self.sira = Queue()
        self.pText = pText  # oluşacak pencerenin açıklama yazısı
        self.pTitle = pTitle  # oluşacak pencerenin başlığı
        self.pBilgi = pBilgi  # oluşacak pencerenin açıklama yazısı
        self.pencere = master  # pencere hangi alana yönelecek

        self.liste = {"whois": self.whois,  # fonksiyonların listesi. işleme göre atanıyor
                      "siteip": self.siteip,
                      "cloudflare": self.cloud,
                      "location": self.location,
                      "honeypot": self.honeypot,
                      "header": self.header,
                      "robot": self.robot,
                      "link": self.link,
                      "trace": self.trace,
                      "zone": self.zone,
                      }

        self.fonk = fonksiyon
        self.rgb = "#008aff"
        self.bg = self.rgb
        self.fg = "black"
        self.buton = Button(master,
                            text=text,
                            font="Helvatica 13 bold",
                            border=0,
                            bg=self.rgb,
                            fg=self.fg,
                            command=self.Pencere)
        self.buton.bind("<Enter>", self.on)
        self.buton.bind("<Leave>", self.off)

        if no >= 0:
            satir = no / 4
            no = no % 4
            self.buton.place(x=((no * 200) + 10),
                             y=(satir * 100) + 210,
                             width=190,
                             height=90)

        else:
            messagebox.showwarning("Hata", "Yerleştirme Hatası")

    def on(self, event=None):
        self.buton["bg"] = self.fg
        self.buton["fg"] = self.bg

    def off(self, event=None):
        self.buton["bg"] = self.bg
        self.buton["fg"] = self.fg

    def demo(self, arg=None):
        messagebox.showwarning("Demo", "Henüz hazır değil")

    def Pencere(self, ara=False):
        if not ara:
            self.top = Toplevel()
            self.top.geometry("450x300+200+200")
            self.top.iconbitmap("image/logo.ico")
            self.top.title(self.pTitle)
            self.top.resizable(width=False, height=False)
            self.top.tk_setPalette("black")
            self.top.transient(self.pencere)

            self.kaydirma = Scrollbar(self.top)
            self.kaydirma.pack(side=RIGHT, fill=Y)

            Label(self.top,
                  text=self.pText,
                  fg=self.rgb,
                  font="Helvatica 13 bold").pack(side=TOP)

            self.bilgi = Label(self.top, text=self.pBilgi)
            self.bilgi.pack(side=TOP)

            self.al = Entry(self.top)
            self.al.pack(side=TOP, fill=X)

            self.pButon = ttk.Button(self.top, text="Başla!", command=partial(self.Pencere, ara=True))
            self.pButon.pack(side=TOP, fill=X)

            self.kutu = Text(self.top, yscrollcommand=self.kaydirma.set, wrap="word", fg="red")
            self.kutu.pack(side=TOP, fill=BOTH)

            self.kaydirma.config(command=self.kutu.yview)
            self.top.mainloop()
        else:
            islem = Thread(target=self.liste[self.fonk], args=(self.sira,))
            islem.daemon = True
            islem.start()
            self.sira.put(self.al.get())
            # self.sira.join()

    def whois(self, que):
        while True:
            try:
                veri = que.get()
                self.bilgi["text"] = "Lütfen Bekleyin..."
                self.buton["state"] = DISABLED
                self.top.protocol("WM_DELETE_WINDOW", lambda: False)
                try:
                    port = "https://api.hackertarget.com/whois/?q=" + veri
                    pport = urlopen(port).read()
                except:
                    que.task_done()
                    self.bilgi["text"] = "Lütfen bilgiyi doğru girin girin"
                    self.buton["state"] = NORMAL
                    self.top.protocol("WM_DELETE_WINDOW", lambda: self.top.destroy())
                    break

                self.kutu.delete(1.0, END)
                self.kutu.insert(END, str(pport))
                que.task_done()
                self.bilgi["text"] = "Tamamlandı"
                self.buton["state"] = NORMAL
                self.top.protocol("WM_DELETE_WINDOW", lambda: self.top.destroy())
                break
            except Exception as e:
                messagebox.showwarning("Hata",
                                       "Kritik bir hata oluştu. Lütfen Programcıya bildirin.\n{}".format(e.message))
                que.task_done()
                self.bilgi["text"] = "Tamamlandı"
                self.buton["state"] = NORMAL
                break

    def siteip(self, que):
        while True:
            try:
                veri = que.get()
                self.bilgi["text"] = "Lütfen Bekleyin..."
                self.buton["state"] = DISABLED
                self.top.protocol("WM_DELETE_WINDOW", lambda: False)
                try:
                    ip = socket.gethostbyname(veri)
                except:
                    que.task_done()
                    self.bilgi["text"] = "Lütfen bilgiyi doğru girin girin"
                    self.buton["state"] = NORMAL
                    self.top.protocol("WM_DELETE_WINDOW", lambda: self.top.destroy())
                    break
                self.kutu.delete(1.0, END)
                self.kutu.insert(END, str(ip))
                que.task_done()
                self.bilgi["text"] = "Tamamlandı"
                self.buton["state"] = NORMAL
                self.top.protocol("WM_DELETE_WINDOW", lambda: self.top.destroy())
                break
            except Exception as e:
                messagebox.showwarning("Hata",
                                       "Kritik bir hata oluştu. Lütfen Programcıya bildirin.\n{}".format(e.message))
                que.task_done()
                self.bilgi["text"] = "Tamamlandı"
                self.buton["state"] = NORMAL
                break

    def honeypot(self, que):
        while True:
            try:
                veri = que.get()
                self.bilgi["text"] = "Lütfen Bekleyin..."
                self.buton["state"] = DISABLED
                self.top.protocol("WM_DELETE_WINDOW", lambda: False)
                try:
                    honey = "https://api.shodan.io/labs/honeyscore/" + str(
                        veri) + "?key=C23OXE0bVMrul2YeqcL7zxb6jZ4pj2by"
                    phoney = urlopen(honey).read()
                except:
                    que.task_done()
                    self.bilgi["text"] = "Lütfen IP adresi girin"
                    self.buton["state"] = NORMAL
                    self.top.protocol("WM_DELETE_WINDOW", lambda: self.top.destroy())
                    break

                self.kutu.delete(1.0, END)
                self.kutu.insert(END, str("Honeypot oranı: %{}".format(float(phoney) * 100)))
                que.task_done()
                self.bilgi["text"] = "Tamamlandı"
                self.buton["state"] = NORMAL
                self.top.protocol("WM_DELETE_WINDOW", lambda: self.top.destroy())
                break
            except Exception as e:
                que.task_done()
                self.bilgi["text"] = "Hata oluştu. Bilgiyi doğru girin"
                self.top.protocol("WM_DELETE_WINDOW", lambda: self.top.destroy())
                self.buton["state"] = NORMAL
                break

    def cloud(self, que):
        while True:
            try:
                veri = que.get()
                self.bilgi["text"] = "Lütfen Bekleyin..."
                self.buton["state"] = DISABLED
                self.top.protocol("WM_DELETE_WINDOW", lambda: False)
                try:
                    ns = "https://api.hackertarget.com/dnslookup/?q=" + veri
                    pns = urlopen(ns).read()
                    if 'cloudflare' in str(pns):
                        sonuc = "CloudFlare Bulundu!\n{}".format(pns)
                    else:
                        sonuc = "CloudFlare Bulunamadı\n{}".format(pns)
                except:
                    que.task_done()
                    self.bilgi["text"] = "Doğru bilgi girmeniz gerekiyor"
                    self.buton["state"] = NORMAL
                    self.top.protocol("WM_DELETE_WINDOW", lambda: self.top.destroy())
                    break

                self.kutu.delete(1.0, END)
                self.kutu.insert(END, str(sonuc))
                que.task_done()
                self.bilgi["text"] = "Tamamlandı"
                self.buton["state"] = NORMAL
                self.top.protocol("WM_DELETE_WINDOW", lambda: self.top.destroy())
                break
            except Exception as e:
                que.task_done()
                self.bilgi["text"] = "Hata oluştu. Bilgiyi doğru girin"
                self.buton["state"] = NORMAL
                self.top.protocol("WM_DELETE_WINDOW", lambda: self.top.destroy())
                break

    def header(self, que):
        while True:
            try:
                veri = que.get()
                self.bilgi["text"] = "Lütfen Bekleyin..."
                self.buton["state"] = DISABLED
                self.top.protocol("WM_DELETE_WINDOW", lambda: False)
                try:
                    header = "http://api.hackertarget.com/httpheaders/?q=" + veri
                    pheader = urlopen(header).read()
                    sonuc = pheader
                except:
                    que.task_done()
                    self.bilgi["text"] = "Doğru bilgi girmeniz gerekiyor"
                    self.buton["state"] = NORMAL
                    self.top.protocol("WM_DELETE_WINDOW", lambda: self.top.destroy())
                    break

                self.kutu.delete(1.0, END)
                self.kutu.insert(END, sonuc)
                que.task_done()
                self.bilgi["text"] = "Tamamlandı"
                self.buton["state"] = NORMAL
                self.top.protocol("WM_DELETE_WINDOW", lambda: self.top.destroy())
                break
            except Exception as e:
                que.task_done()
                self.bilgi["text"] = "Hata oluştu. Bilgiyi doğru girin"
                self.buton["state"] = NORMAL
                self.top.protocol("WM_DELETE_WINDOW", lambda: self.top.destroy())
                break

    def robot(self, que):
        while True:
            try:
                veri = que.get()
                self.bilgi["text"] = "Lütfen Bekleyin..."
                self.buton["state"] = DISABLED
                self.top.protocol("WM_DELETE_WINDOW", lambda: False)
                try:
                    if veri.startswith("http") or veri.startswith("https"):
                        robot = veri + "/robots.txt"
                        probot = urlopen(robot).read()
                        sonuc = probot
                    else:
                        que.task_done()
                        self.bilgi["text"] = "Doğru bilgi girmeniz gerekiyor"
                        self.buton["state"] = NORMAL
                        self.top.protocol("WM_DELETE_WINDOW", lambda: self.top.destroy())
                        break
                except Exception as e:
                    print(e.message)
                    que.task_done()
                    self.bilgi["text"] = "Site izni yok yada yanlış site"
                    self.buton["state"] = NORMAL
                    self.top.protocol("WM_DELETE_WINDOW", lambda: self.top.destroy())
                    break

                self.kutu.delete(1.0, END)
                self.kutu.insert(END, sonuc)
                que.task_done()
                self.bilgi["text"] = "Tamamlandı"
                self.buton["state"] = NORMAL
                self.top.protocol("WM_DELETE_WINDOW", lambda: self.top.destroy())
                break
            except Exception as e:
                que.task_done()
                self.bilgi["text"] = "Hata oluştu. Bilgiyi doğru girin"
                self.buton["state"] = NORMAL
                self.top.protocol("WM_DELETE_WINDOW", lambda: self.top.destroy())
                break

    def zone(self, que):
        while True:
            try:
                veri = que.get()
                self.bilgi["text"] = "Lütfen Bekleyin..."
                self.buton["state"] = DISABLED
                self.top.protocol("WM_DELETE_WINDOW", lambda: False)
                try:
                    zone = "http://api.hackertarget.com/zonetransfer/?q=" + veri
                    try:
                        pzone = urlopen(zone).read()
                        sonuc = pzone
                    except 'failed' in sonuc:
                        sonuc += "\nZone transfer failed"

                except Exception as e:
                    print(e.message)
                    que.task_done()
                    self.bilgi["text"] = "Site izni yok yada yanlış site"
                    self.buton["state"] = NORMAL
                    self.top.protocol("WM_DELETE_WINDOW", lambda: self.top.destroy())
                    break

                self.kutu.delete(1.0, END)
                self.kutu.insert(END, sonuc)
                que.task_done()
                self.bilgi["text"] = "Tamamlandı"
                self.buton["state"] = NORMAL
                self.top.protocol("WM_DELETE_WINDOW", lambda: self.top.destroy())
                break
            except Exception as e:
                que.task_done()
                self.bilgi["text"] = "Hata oluştu. Bilgiyi doğru girin"
                self.buton["state"] = NORMAL
                self.top.protocol("WM_DELETE_WINDOW", lambda: self.top.destroy())
                break

    def link(self, que):
        while True:
            try:
                veri = que.get()
                self.bilgi["text"] = "Lütfen Bekleyin..."
                self.buton["state"] = DISABLED
                self.top.protocol("WM_DELETE_WINDOW", lambda: False)
                try:
                    crawl = "https://api.hackertarget.com/pagelinks/?q=" + veri
                    pcrawl = urlopen(crawl).read()
                    sonuc = pcrawl
                except Exception as e:
                    print(e.message)
                    que.task_done()
                    self.bilgi["text"] = "Site izni yok yada yanlış site"
                    self.buton["state"] = NORMAL
                    self.top.protocol("WM_DELETE_WINDOW", lambda: self.top.destroy())
                    break

                self.kutu.delete(1.0, END)
                self.kutu.insert(END, sonuc)
                que.task_done()
                self.bilgi["text"] = "Tamamlandı"
                self.buton["state"] = NORMAL
                self.top.protocol("WM_DELETE_WINDOW", lambda: self.top.destroy())
                break
            except Exception as e:
                que.task_done()
                self.bilgi["text"] = "Hata oluştu. Bilgiyi doğru girin"
                self.buton["state"] = NORMAL
                self.top.protocol("WM_DELETE_WINDOW", lambda: self.top.destroy())
                break

    def trace(self, que):
        while True:
            try:
                veri = que.get()
                self.bilgi["text"] = "Lütfen Bekleyin..."
                self.buton["state"] = DISABLED
                self.top.protocol("WM_DELETE_WINDOW", lambda: False)
                try:
                    trace = "https://api.hackertarget.com/mtr/?q=" + veri
                    ptrace = urlopen(trace).read()
                    sonuc = ptrace
                except Exception as e:
                    print(e.message)
                    que.task_done()
                    self.bilgi["text"] = "Site izni yok yada yanlış site"
                    self.buton["state"] = NORMAL
                    self.top.protocol("WM_DELETE_WINDOW", lambda: self.top.destroy())
                    break

                self.kutu.delete(1.0, END)
                self.kutu.insert(END, sonuc)
                que.task_done()
                self.bilgi["text"] = "Tamamlandı"
                self.buton["state"] = NORMAL
                self.top.protocol("WM_DELETE_WINDOW", lambda: self.top.destroy())
                break
            except Exception as e:
                que.task_done()
                self.bilgi["text"] = "Hata oluştu. Bilgiyi doğru girin"
                self.buton["state"] = NORMAL
                self.top.protocol("WM_DELETE_WINDOW", lambda: self.top.destroy())
                break

    def location(self, que):
        while True:
            try:
                veri = que.get()
                self.bilgi["text"] = "Lütfen Bekleyin..."
                self.buton["state"] = DISABLED
                self.top.protocol("WM_DELETE_WINDOW", lambda: False)
                try:
                    trace = "https://ipapi.co/{}/json/".format(veri)
                    ptrace = u.request.urlopen(trace)
                    snc = json.load(ptrace)
                    sonuc = ""
                    sonuc += "Şehir: {}\n".format(snc["city"])
                    sonuc += "IP: {}\n".format(snc["ip"])
                    sonuc += "Ülke: {}\n".format(snc["country_name"])
                    sonuc += "Bölge: {}\n".format(snc["region"])
                except Exception as e:
                    print(e.message)
                    que.task_done()
                    self.bilgi["text"] = "Bir Hata meydana geldi"
                    self.buton["state"] = NORMAL
                    self.top.protocol("WM_DELETE_WINDOW", lambda: self.top.destroy())
                    break

                self.kutu.delete(1.0, END)
                self.kutu.insert(END, sonuc)
                que.task_done()
                self.bilgi["text"] = "Tamamlandı"
                self.buton["state"] = NORMAL
                self.top.protocol("WM_DELETE_WINDOW", lambda: self.top.destroy())
                break
            except Exception as e:
                que.task_done()
                self.bilgi["text"] = "Hata oluştu. Bilgiyi doğru girin"
                self.buton["state"] = NORMAL
                self.top.protocol("WM_DELETE_WINDOW", lambda: self.top.destroy())
                break
