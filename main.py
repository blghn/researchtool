﻿# !env/bin/python3
# coding: utf-8
# @author:blghn - fnym
from urllib.request import urlopen as u
from tkinter import *
from tkinter import ttk
import time
import PIL.ImageTk
import PIL.Image
from functools import partial
import os
from tkinter import messagebox
from urllib import *
from threading import Thread
from window import *
import sys


def get_dir(src):
    dir = sys.argv[0]
    dir = dir.split('/')
    dir.pop(-1)
    dir = '/'.join(dir)
    dir = dir + '/' + src
    return dir

print(get_dir("image/logo.ico"))

########################################################################################## İNTERNET BAĞLANTISI KONTROL

def netControl():
    try:
        u.request.urlopen("http://example.com")
        return True
    except Exception as e:
        print(e.message)
        return False


if not netControl():
    messagebox.showwarning("Hata", "Bu programı şu an internet bağlantısı olmadan kullanamazsınız!")
    sys.exit(0)


############################################################################################


class WebHackTool:
    def __init__(self):  # Pencereyi Oluşturuyoruz
        self.pencere = Tk()
        self.rgb = "#008aff"
        # ortalamak için
        self.h = ((self.pencere.winfo_screenheight()) // 2) - (142 // 2)
        self.w = ((self.pencere.winfo_screenwidth()) // 2) - (712 // 2)
        self.pencere.overrideredirect(1)
        self.pencere.resizable(width=False, height=False)
        self.pencere.geometry("712x142+{}+{}".format(self.w, self.h))
        self.pencere.title("WebHackTool 1.0")
        self.pencere.iconbitmap("@image/logo.ico")
        self.img = PIL.ImageTk.PhotoImage(PIL.Image.open("image/banner.png"))
        self.panel = Label(self.pencere, image=self.img)
        self.panel.pack(side="bottom", fill="both", expand="yes")
        # self.pencere.after(0, partial(self.efekt, 0.1, 0, durum=1))
        self.pencere.after(1500, self.start)
        self.pencere.mainloop()

    def efekt(self, alfa, sayac=0, durum=0, event=None):  # efektli açılış ekranı
        if sayac < 1:
            if durum:
                self.pencere.wm_attributes('-alpha', alfa)
                alfa += 0.1
                if alfa >= 0.9:
                    durum = 0
                    self.pencere.after(50, partial(self.efekt, 0.9, sayac + 1, durum))
                else:
                    self.pencere.after(50, partial(self.efekt, alfa, sayac, durum))
            else:
                self.pencere.wm_attributes('-alpha', alfa)
                alfa -= 0.1
                if alfa <= 0.0:
                    durum = 1
                    self.pencere.after(50, partial(self.efekt, alfa, sayac, durum))
                else:
                    self.pencere.after(50, partial(self.efekt, alfa, sayac, durum))
        else:
            self.pencere.wm_attributes('-alpha', 1)

    def start(self):  # Pencere açılırken yapılan eylemler

        self.h = ((self.pencere.winfo_screenheight()) // 2) - 300
        self.w = ((self.pencere.winfo_screenwidth()) // 2) - 400
        self.panel.destroy()

        self.img = PIL.ImageTk.PhotoImage(PIL.Image.open("image/background.png"))
        self.panel = Label(self.pencere, image=self.img)
        self.panel.place(x=0, y=0)

        self.pencere.wm_attributes('-alpha', 1)
        self.pencere.geometry("810x600+{}+{}".format(self.w, self.h))
        self.pencere.overrideredirect(False)
        self.pencere.tk_setPalette("black")

        Thread(target=self.ip, args=(), ).start()
        self.banner = Label(self.pencere,
                            text="© WebHackTool 1.0",
                            bg=self.rgb,
                            fg="black")
        self.banner.pack(side=BOTTOM, fill=X)

        self.islemListe = [{"buton": "Whois Çekme",  # Bu kısım da butonları sıralıyoruz
                            # "pencere":self.Pencere,
                            "title": "WebHackTool Whois",
                            "text": "Whois bilgisi çekme",
                            "bilgi": "IP adresi yada Domain",
                            "fonk": "whois"},

                           {"buton": "Site Ip Bulma",
                            # "pencere":self.Pencere,
                            "title": "WebHackTool SiteIp",
                            "text": "Hedef sistem ip tespiti",
                            "bilgi": "Domain Name",
                            "fonk": "siteip"},

                           {"buton": "CloudFlare\nTespiti",
                            # "pencere":self.Pencere,
                            "title": "WebHackTool CloudFlare",
                            "text": "Hedefte CloudFlare Tespiti",
                            "bilgi": "IP adresi yada Domain",
                            "fonk": "cloudflare"},

                           {"buton": "IP location",
                            # "pencere":self.Pencere,
                            "title": "WebHackTool IPlocation",
                            "text": "IP adresinden yer bulma",
                            "bilgi": "IP adresi girin:",
                            "fonk": "location"},

                           {"buton": "HoneyPot",
                            # "pencere":self.Pencere,
                            "title": "WebHackTool HoneyPot",
                            "text": "Hedef sistemde HoneyPot oranı",
                            "bilgi": "IP adresi",
                            "fonk": "honeypot"},

                           {"buton": "HTTP Header Grabber",
                            # "pencere":self.Pencere,
                            "title": "WebHackTool HeaderGrabber",
                            "text": "Web sitesi başlık bilgileri",
                            "bilgi": "IP adresi yada Domain",
                            "fonk": "header"},

                           # ["Port Scan",self.Pencere,"WebHackTool PortScan","Hedef sistem port tarama","IP adresi yada Domain"],
                           {"buton": "Robots.txt",
                            # "pencere":self.Pencere,
                            "title": "WebHackTool robots.txt",
                            "text": "Hedef sistemde robots.txt tespiti",
                            "bilgi": "Domain (http(s)://) ile yazın",
                            "fonk": "robot"},

                           {"buton": "Link Grabber",
                            # "pencere":self.Pencere,
                            "title": "WebHackTool LinkGrabber",
                            "text": "Hedef sistemde link taraması",
                            "bilgi": "IP adresi yada Domain",
                            "fonk": "link"},

                           {"buton": "Traceroute",
                            # "pencere":self.Pencere,
                            "title": "WebHackTool TraceRoute",
                            "text": "Hedef sisteme giden yolu izleme",
                            "bilgi": "IP adresi yada Domain",
                            "fonk": "trace"},

                           {"buton": "Zone Transfer",
                            # "pencere":self.Pencere,
                            "title": "WebHackTool ZoneTransfer",
                            "text": "Hedef sistem zone tespiti",
                            "bilgi": "IP adresi yada Domain",
                            "fonk": "zone"},

                           ]

        sira = 0

        for i in self.islemListe:  # bu kısımda butonlara bilgilerini ve fonksiyonları atıyoruz.
            Window1(master=self.pencere,
                    no=sira,
                    text=i["buton"],
                    pTitle=i["title"],
                    pText=i["text"],
                    pBilgi=i["bilgi"],
                    # command = i["pencere"],
                    fonksiyon=i["fonk"] or None)
            sira += 1
            if sira >= len(self.islemListe):
                break

        hakkindaB = Window1(master=self.pencere,  # hakkında butonu
                            no=sira,
                            text="Hakkında/Beni Oku",
                            pTitle="Hakkında",
                            pText="Hakkında",
                            pBilgi="Hakkında")

        hakkindaB.buton["command"] = self.hakkinda

        cikisB = Window1(master=self.pencere,  # Çıkış butonu
                         no=sira + 1,
                         text="Çıkış",
                         pTitle="Çıkış",
                         pText="Çıkış",
                         pBilgi="Çıkış")

        cikisB.buton["command"] = self.cik

    def ip(self):  # banner (alt kısım) bölümü
        ipAdres = u.request.urlopen("http://ipv4bot.whatismyipaddress.com").read()
        self.banner["text"] = self.banner["text"] + " | IP: {}".format(ipAdres)

    def hakkinda(self):
        mesaj = "WebHackTool 1.0"
        messagebox.showinfo("WebHackTool", mesaj)

    def cik(self):
        self.pencere.destroy()
        sys.exit(0)


WebHackTool()
